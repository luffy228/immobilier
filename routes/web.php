<?php

use App\Http\Controllers\ArrondissementController;
use App\Http\Controllers\EnginTypeController;
use App\Http\Controllers\ForceTypeController;
use App\Http\Controllers\MotifController;
use App\Http\Controllers\ObservationController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route test
Route::post('/admin', 'HomeController@adminPage')->name('admin');
Route::get('/','TestController@index');
Route::get('/addAdmin', 'RedirectionController@addAdmin')->name('addadmin');
Route::get('/listAlerts', 'RedirectionController@listAlert')->name('alertList');
Route::get('/addObservations', 'RedirectionController@addObservations')->name('addObs');
Route::get('/listObservations', 'RedirectionController@listObservations')->name('listObs');

Route::prefix("/engin")->group(function () {
    //Route::post('/add', [TypeServiceController::class, 'add']);
    //Route::post('/delete', [TypeServiceController::class, 'delete']);
    //Route::post('/update', [TypeServiceController::class, 'update']);
    //Route::get('/list', [TypeServiceController::class, 'all']);
    Route::get('/show',[EnginTypeController::class,'show'])->name('listengins');;;
    //Route::post("/services", [TypeServiceController::class, 'getAllServices']);
});

Route::prefix("/motif")->group(function () {
    //Route::post('/add', [TypeServiceController::class, 'add']);
    //Route::post('/delete', [TypeServiceController::class, 'delete']);
    //Route::post('/update', [TypeServiceController::class, 'update']);
    //Route::get('/list', [TypeServiceController::class, 'all']);
    Route::get('/show',[MotifController::class,'show'])->name('listmotifs');
    //Route::post("/services", [TypeServiceController::class, 'getAllServices']);
});

Route::prefix("/force")->group(function () {
    //Route::post('/add', [TypeServiceController::class, 'add']);
    //Route::post('/delete', [TypeServiceController::class, 'delete']);
    //Route::post('/update', [TypeServiceController::class, 'update']);
    //Route::get('/list', [TypeServiceController::class, 'all']);
    Route::get('/show',[ForceTypeController::class,'show']);
    //Route::post("/services", [TypeServiceController::class, 'getAllServices']);
});

Route::prefix("/arrondissement")->group(function () {
    //Route::post('/add', [TypeServiceController::class, 'add']);
    //Route::post('/delete', [TypeServiceController::class, 'delete']);
    //Route::post('/update', [TypeServiceController::class, 'update']);
    //Route::get('/list', [TypeServiceController::class, 'all']);
    Route::get('/showGendarmerie',[ArrondissementController::class,'showGendarmerie']);
    Route::get('/showPolice',[ArrondissementController::class,'showPolice']);
    //Route::post("/services", [TypeServiceController::class, 'getAllServices']);
});

Route::prefix("/observation")->group(function () {
    //Route::post('/add', [TypeServiceController::class, 'add']);
    //Route::post('/delete', [TypeServiceController::class, 'delete']);
    //Route::post('/update', [TypeServiceController::class, 'update']);
    //Route::get('/list', [TypeServiceController::class, 'all']);
    Route::get('/showGendarmerie',[ObservationController::class,'observationgendarmerie']);

    //Route::post("/services", [TypeServiceController::class, 'getAllServices']);
});

Route::prefix("/superadmin")->group(function () {
    //Route::post('/add', [TypeServiceController::class, 'add']);
    //Route::post('/delete', [TypeServiceController::class, 'delete']);
    //Route::post('/update', [TypeServiceController::class, 'update']);
    //Route::get('/list', [TypeServiceController::class, 'all']);
    Route::get('/adminPolice',[UserController::class,'adminpolice']);
    Route::get('/adminGendarmerie',[UserController::class,'admingendarmerie']);
    Route::get('/sousGendarmerie',[UserController::class,'sousadmingendarmerie']);
    Route::get('/sousPolice',[UserController::class,'sousadminpolice']);
    Route::get('/controleurGendarmerie',[UserController::class,'controlleurgendarmerie']);
    //Route::post("/services", [TypeServiceController::class, 'getAllServices']);
});

//Auth::routes();


