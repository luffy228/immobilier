<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBienAppartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bien_apparts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('proprio_id');
            $table->foreign('proprio_id')->references('id')->on('proprios');
            $table->unsignedBigInteger('bien_id');
            $table->foreign('bien_id')->references('id')->on('biens');
            $table->string('demande');
            $table->string('statut');
            $table->integer('prix');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bien_apparts');
    }
}
