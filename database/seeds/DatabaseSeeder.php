<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(
            [
                CountrySeeder::class,
                PermissionSeeder::class,
                RoleSeeder::class,
                RolePermissionSeeder::class,
                EnginSeeder::class,
                ForceTypeSeeder::class,
                MotifSeeder::class,
                SuperAdminSeeder::class,
                VisibilitySeeder::class,
            ]
        );

    }
}
