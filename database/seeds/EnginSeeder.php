<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EnginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('engin_types')->insertOrIgnore([
            ['description' => 'Moto'],
            ['description' => 'Voiture'],
        ]);
    }
}
