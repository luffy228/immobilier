<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VisibilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('visibilities')->insertOrIgnore([
            ['description' => 'Gendarmerie'],
            ['description' => 'Police'],
            ['description' => 'All'],
        ]);
    }
}
