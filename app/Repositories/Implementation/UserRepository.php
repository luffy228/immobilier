<?php

namespace  App\Repositories\Implementation;

use App\Repositories\Generic\GenericImplementation\GenericRepository;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserRepository extends GenericRepository
{
    use ApiResponser;
    protected $rules = [
    ];


    public function model()
    {
        return 'App\Models\User';
    }

    /**
     * validate data from request
     *
     * @param $rules Array of rules
     * @param $messages Array of messages
     * @return Instance of Validator
     */
    public function validateData()
    {
        $valider =  Validator::make(request()->all(),$this->rules);
        if($valider->fails()) {
              return $this->errorExceptionResponse($valider->errors()->all(), 'VALIDATION_ERROR', 402);
        }
    }

    public function information($userId)
    {
        $record = $this->model
                        ->where('id',$userId)
                        ->first();
        return $record;
    }

    public function listesousAdmin($name,$role)
    {
        $record = DB::table('users')
                        ->where('type',$name)
                        ->join('model_has_roles','users.id','=','model_has_roles.model_id')
                        ->where('model_has_roles.role_id',$role)
                        ->whereNull('users.user_id')
                        ->select('id','name','firstname','username','email','telephone','type')
                        ->get();
        return $record;
    }

    public function listcontrolleur($name,$role)
    {
        $record = DB::table('users')
                        ->where('type',$name)
                        ->join('model_has_roles','users.id','=','model_has_roles.model_id')
                        ->where('model_has_roles.role_id',$role)
                        ->join('agents','users.user_id','=','agents.id')
                        ->join('arrondissements','agents.arrondissements_id','=','arrondissements.id')
                        ->select('agents.id','name','firstname','username','email','users.telephone','users.type','arrondissements.numero')
                        ->get();
        return $record;
    }

    public function listeAdmin($name,$role)
    {
        $record = DB::table('users')
                        ->where('type',$name)
                        ->join('model_has_roles','users.id','=','model_has_roles.model_id')
                        ->where('model_has_roles.role_id',$role)
                        ->select('id','name','firstname','username','email','telephone','type')
                        ->get();
        return $record;
    }




}
