<?php

namespace  App\Repositories\Generic\GenericImplementation;

use App\Repositories\Generic\GenericInterface\GenericRepositoryInterface;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;


abstract class GenericRepository implements GenericRepositoryInterface
{
    private $app;
    protected $model;

    /**
     * @param App $app
     * @throws
     */

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }
    /**
     * specification du model
     * @return mixed
     */

    abstract function model();

    public function all()
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update(array $data, $id)
    {
        $record = $this->model->find($id);
        return $record->update($data);
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws ReposittoryException
     *
    */


   public function makeModel() {
       $model = app($this->model());


       if (!$model instanceof Model)
           //throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
           dd("ERROR");

       return $this->model = $model->newQuery();
   }


}
