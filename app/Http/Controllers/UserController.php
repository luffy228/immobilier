<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\ClientsRepository;
use App\Repositories\Implementation\ProprioRepository;
use App\Repositories\Implementation\UserRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    //
    protected $userRepo;
    protected $clientRepo;
    protected $proprioRepo;
    use ApiResponser;
     function __construct(App $app)
    {
        $this->userRepo = new UserRepository($app);
        $this->clientRepo = new ClientsRepository($app);
        $this->proprioRepo = new ProprioRepository($app);
    }

    public function logout()
    {
        $token = Auth::guard('api')->user()->token();
        $token->revoke();
        return $this->successResponse(null, 'You have been successfully logged out!', 201);
    }

    public function login()
    {
        if(Auth::attempt(['username' => request('name'), 'password' => request('password')])){
            $user = Auth::user();
            $data['token'] =  $user->createToken('token')->accessToken;
            $data['information'] = $this->userRepo->Information($user->id);
            return $this->successResponse($data,'', 200);
        }
        else{
            return $this->errorResponse('Authentification failled: email or password incorrect', 403);
        }
    }

    public function register(Request $request)
    {
        $this->userRepo->validateData();

            $form_request = [
                'name'=>$request["name"],
                'username'=>$request["username"],
                'password'=>bcrypt($request["password"]),
                'email'=>$request["email"],
                'telephone'=>$request["telephone"],
            ];
            $user = $this->userRepo->create($form_request);
            if ($request["type"] == "client") {
                $client_request = [];
                $client = $this->clientRepo->create($client_request);
                $userRequest =
                [
                    'user_type'=>$this->clientRepo->model(),
                    'user_id'=>$client->id
                ];
                $this->userRepo->update($userRequest,$user->id);
                $data['token'] =  $user->createToken('token')->accessToken;
                $data['information'] = $this->userRepo->Information($user->id);
                return $this->successResponse($data,'Inscription reussi avec success', 200);
            }
            if ($request["type"] == "proprio") {

                $proprio_request = [];
                $proprio = $this->proprioRepo->create($proprio_request);
                $userRequest =
                [
                    'user_type'=>$this->proprioRepo->model(),
                    'user_id'=>$proprio->id
                ];
                $this->userRepo->update($userRequest,$user->id);
                $data['token'] =  $user->createToken('token')->accessToken;
                $data['information'] = $this->userRepo->Information($user->id);
                return $this->successResponse($data,'Inscription reussi avec success', 200);
            }




    }








}
