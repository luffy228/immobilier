<?php

namespace App\Http\Controllers;

use App\Repositories\Implementation\PermissionRepository;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class PermissionController extends Controller
{
    //
    protected $permissionRepo;
    use ApiResponser;
     function __construct(App $app)
    {
        $this->permissionRepo =new PermissionRepository($app);
    }
    public function all()
    {
        $permission = $this->permissionRepo->all();
        return $this->successResponse($permission,"Liste des permissions",201);
    }


}
