<?php

namespace App\Http\Controllers;

use App\Repositories\Implementation\AgentRepository;
use App\Repositories\Implementation\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    protected $userRepo;
    protected $agentRepo;
    function __construct(App $app)
    {
        $this->userRepo = new UserRepository($app);
        $this->agentRepo = new AgentRepository($app);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
  /*   public function __construct()
    {
        $this->middleware('auth');
    } */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminPage()
    {
        if(Auth::attempt(['username' => request('name'), 'password' => request('password')])){
            $user = Auth::user();
            $data['token'] =  $user->createToken('token')->accessToken;
            $data['role'] = $user->getRoleNames();
            $data['information'] = $this->userRepo->Information($user->id);
            return view('admin',compact('data'));
            //$data['role'][0] pour avoir le role d'un utilisateur
        }
        else{
            //return $this->errorResponse('Authentification failled: email or password incorrect', 403);
        }
        //
    }
}
