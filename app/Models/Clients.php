<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    public function userMother()
    {
        return $this->morphOne('App\Models\User','userType');
    }

    public function nameModel()
    {
        return 'Clients';
    }
}
