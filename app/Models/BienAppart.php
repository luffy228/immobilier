<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BienAppart extends Model
{
    protected $fillable = [
        'demande','statut','prix','proprio_id','bien_id'
    ];


    public function nameModel()
    {
        return 'BienAppart';
    }
}
