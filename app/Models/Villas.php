<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Villas extends Model
{
    //
    public function bienMother()
    {
        return $this->morphOne('App\Models\Biens','bienType');
    }

    public function nameModel()
    {
        return 'Villas';
    }
}
