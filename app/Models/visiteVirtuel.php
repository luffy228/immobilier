<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class visiteVirtuel extends Model
{
    protected $fillable = [
        'proprio_id','bien_id'
    ];


    public function nameModel()
    {
        return 'visiteVirtuel';
    }
}
