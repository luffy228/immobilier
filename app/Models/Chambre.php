<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chambre extends Model
{
    //
    public function bienMother()
    {
        return $this->morphOne('App\Models\Biens','bienType');
    }

    public function nameModel()
    {
        return 'Chambre';
    }
}
