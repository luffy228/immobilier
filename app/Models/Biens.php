<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Biens extends Model
{
    protected $fillable = [
        'quartier','piece','description','biens_type','biens_id'
    ];

    public function bienType()
    {
        return $this->morphTo();
    }
    public function nameModel()
    {
        return 'Biens';
    }
}
