@extends('layouts.app')

@section('contenu')
<div id="exampleModalCenter" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Modal Title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <p class="mb-0">Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn  btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<h2 class=" text-center mb-3 font-weight-bold">Listes des motifs</h2>
<div class="col-md-12">
    @foreach ($motifs as $motif)
        <div class="row">
            <div class="col-sm-3">
                <div class="card">
                    <h3 class="text-center" style="color: green">{{$motif["description"]}}</h3>
                </div>

            </div>
        </div>
    @endforeach

</div>
@endsection
