@extends('layouts.app')

@section('contenu')
<div class="col-sm-12">

    <div class="card">
        <div class="card-body">

        <h2 class=" text-center mb-3 font-weight-bold">Liste des observations</h2>
        <button  type="button" class="btn btn-success float-left mb-3 ml-4"><i class="feather mr-2 icon-check-circle"></i>Ajouter une observation</button>
        <div class="card-body table-border-style">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Engin</th>
                            <th>Serie</th>
                            <th>Plaque</th>
                            <th>Motif</th>
                            <th>Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>

                            <td>Mark</td>
                            <td>Otto</td>
                            <td>
                               
                            </td>
                        </tr>
                        <tr>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>

                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>

                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
