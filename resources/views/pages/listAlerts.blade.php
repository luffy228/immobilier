@extends('layouts.app')

@section('contenu')
<div id="exampleModalCenter" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Modal Title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <p class="mb-0">Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn  btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<h2 class=" text-center mb-3 font-weight-bold">Listes des alertes</h2>
<div class="col-md-12">
    <div class="row">
        <div class="col-sm-3">
            <div class="card">
<h3 class="text-center" style="color: green">Voiture 3658 BD</h3>
    <div class="card-body text-size">
            <p>Couleur:Grise</p>
            <p>Pays:Togo</p>
            <p>Numero de permis:924-785-875</p>
    
            <div class="row">
                <button type="button" class="btn btn-success has-ripple" data-toggle="modal" data-target="#exampleModalCenter">voir plus<span class="ripple ripple-animate" style="height: 174.078px; width: 174.078px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -57.031px; left: 29.6409px;"></span></button>
            <button class="btn btn-danger has-ripple  ml-2">Supprimer<span class="ripple ripple-animate" style="height: 82.2656px; width: 82.2656px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -16.2344px; left: 12.3281px;"></span></button>
            <button class="btn btn-primary has-ripple  ml-2">Partager<span class="ripple ripple-animate" style="height: 82.2656px; width: 82.2656px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -16.2344px; left: 12.3281px;"></span></button>
        </div>
    </div>
            </div>
    </div>
    <div class="col-sm-3">
        <div class="card">
<h3 class="text-center" style="color: green">Voiture 3658 BD</h3>
<div class="card-body text-size">
        <p>Couleur:Grise</p>
        <p>Pays:Togo</p>
        <p>Numero de permis:924-785-875</p>

    <div class="row">
        <button class="btn btn-success has-ripple ml-1 ">Voir plus<span class="ripple ripple-animate" style="height: 108.922px; width: 108.922px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -28.5704px; left: 14.539px;"></span></button>
        <button class="btn btn-danger has-ripple  ml-2">Supprimer<span class="ripple ripple-animate" style="height: 82.2656px; width: 82.2656px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -16.2344px; left: 12.3281px;"></span></button>
        <button class="btn btn-primary has-ripple  ml-2">Partager<span class="ripple ripple-animate" style="height: 82.2656px; width: 82.2656px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -16.2344px; left: 12.3281px;"></span></button>
    </div>
</div>
        </div>
</div>
<div class="col-sm-3">
    <div class="card">
<h3 class="text-center" style="color: green">Voiture 3658 BD</h3>
<div class="card-body text-size">
    <p>Couleur:Grise</p>
    <p>Pays:Togo</p>
    <p>Numero de permis:924-785-875</p>

<div class="row">
    <button class="btn btn-success has-ripple ">Voir plus<span class="ripple ripple-animate" style="height: 108.922px; width: 108.922px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -28.5704px; left: 14.539px;"></span></button>
    <button class="btn btn-danger has-ripple  ml-2">Supprimer<span class="ripple ripple-animate" style="height: 82.2656px; width: 82.2656px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -16.2344px; left: 12.3281px;"></span></button>
    <button class="btn btn-primary has-ripple  ml-2">Partager<span class="ripple ripple-animate" style="height: 82.2656px; width: 82.2656px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -16.2344px; left: 12.3281px;"></span></button>
</div>
</div>
    </div>
</div>
<div class="col-sm-3">
    <div class="card">
<h3 class="text-center" style="color: teal">Moto 3658 BD</h3>
<div class="card-body text-size">
    <p>Couleur:Grise</p>
    <p>Pays:Togo</p>
    <p>Motif:Vol</p>
<div class="row">
    <button class="btn btn-success has-ripple ">Voir plus<span class="ripple ripple-animate" style="height: 108.922px; width: 108.922px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -28.5704px; left: 14.539px;"></span></button>
    <button class="btn btn-danger has-ripple  ml-2">Supprimer<span class="ripple ripple-animate" style="height: 82.2656px; width: 82.2656px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -16.2344px; left: 12.3281px;"></span></button>
    <button class="btn btn-primary has-ripple  ml-2">Partager<span class="ripple ripple-animate" style="height: 82.2656px; width: 82.2656px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -16.2344px; left: 12.3281px;"></span></button>
</div>
</div>
    </div>
</div>

    </div>
</div>
@endsection
