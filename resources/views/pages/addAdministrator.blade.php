@extends('layouts.app')

@section('contenu')
<div class="col-sm-12">
    <div class="card">

        <div class="card-body">

        <h2 class=" text-center mb-3 font-weight-bold">Enregistrer un Administrateur</h2>
        <form>


                <div class="col-sm-10 offset-1">
                    <div class="form-group">
                        <label class="floating-label" for="Nom">Nom</label>
                        <input type="text" class="form-control" id="Nom" placeholder="">
                    </div>
                </div>
                <div class="col-sm-10 offset-1">
                    <div class="form-group">
                        <label class="floating-label" for="prenoms">prenom(s)</label>
                        <input type="text" class="form-control" id="prenoms" placeholder="">
                    </div>
                </div>


            <div class="mt-5">
                <div class="col-sm-10 offset-1">
                    <div class="form-group">
                        <label class="floating-label" for="Telephone">Telephone</label>
                        <input type="text" class="form-control" id="Telephone" placeholder="">
                    </div>
                </div>
                <div class="col-sm-10 offset-1">
                    <div class="form-group">
                        <label class="floating-label" for="email">Email</label>
                        <input type="email" class="form-control" id="email" placeholder="">
                    </div>
                </div>

            </div>
            <div class=" mt-5">
                <div class="col-sm-10 offset-1">
                    <div class="form-group">
                        <label class="floating-label" for="password">Mot de passe</label>
                        <input type="password" class="form-control" id="password" placeholder="">
                    </div>
                </div>
                <div class="col-sm-10 offset-1">
                    <div class="form-group">
                        <label class="floating-label" for="c_password">Confirmer le mot de passe</label>
                        <input type="password" class="form-control" id="c_password" placeholder="">
                    </div>
                </div>
            </div>

            <div class="mt-5">
                <div class="col-sm-10 offset-1">
                    <div class="form-group">
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option value=""selected>Veuillez selectionez le grade</option>
                            <option value="option2" >2</option>
                            <option value="option3">3</option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="text-center mt-4">
                <button type="button" class="btn btn-lg btn-info text-center">Enregister l'administrateur </button>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
