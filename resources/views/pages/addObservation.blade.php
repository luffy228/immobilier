@extends('layouts.app')

@section('contenu')
<div class="col-sm-12">
    <div class="card">
        <div class="card-body">

        <h2 class=" text-center mb-3 font-weight-bold">Enregistrer une observation</h2>
        <form>
            <div class="col-sm-10 offset-1">
                <div class="row">
                    <div class=" form-group col-sm-2  ">
                        <div class="form-group">
                            <label class="floating-label" for="serie">Serie</label>
                            <input type="text" class="form-control" id="serie" placeholder="">
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <label class="floating-label" for="plaque">Numero de la plaque</label>
                            <input type="text" class="form-control" id="plaque" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-sm-10 offset-1">
                    <div class="form-group">
                        <label class="floating-label" for="permis">Numero de permis</label>
                        <input type="text" class="form-control" id="permis" placeholder="">
                    </div>
                </div>


            <div class="mt-5">
                <div class="col-sm-10 offset-1">
                    <div class="form-group">
                        <label class="floating-label" for="Couleur">Couleur</label>
                        <input type="text" class="form-control" id="Couleur" placeholder="">
                    </div>
                </div>
            </div>
            <div class="col-sm-10 offset-1 mt-5">
                <div class="row">
                    <div class="form-group col-sm-6">
                        
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option value="" selected>Type d'engin</option>
                            <option value="option2" >2</option>
                            <option value="option3">3</option>

                        </select>
                    
                </div>
                    <div class="form-group col-sm-6">
                        
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option value="" selected>Motif de l'observation</option>
                                <option value="option2" >2</option>
                                <option value="option3">3</option>
    
                            </select>
                        
                    </div>
                </div>
            </div>
            <div class="col-sm-10 offset-1 mt-5">
                <div class="row">
                    <div class="form-group col-sm-6">
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option value="" selected>Selectionnez le pays</option>
                            <option value="option2" >2</option>
                            <option value="option3">3</option>

                        </select>
                    
                    </div>
                    <div class="col-sm-3 mt-4 offset-1">
                        <div class="  custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label" for="customCheck1">Autoriser les autres types</label>
                        </div>
                    </div>
                </div>

                
            </div>

            <div class="text-center mt-4">
                <button type="button" class="btn btn-lg btn-info text-center">Enregister l'observation</button>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
