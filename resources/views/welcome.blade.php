<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="{{ asset('css/login.css') }}">
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">

    </head>
    <body>
        <div class="container">
            <div class="card card-login mx-auto text-center bg-dark">
                <div class="card-header mx-auto bg-dark">
                    <span style="font-size: 60px;color:green;font-weight: bold"> SSR </span><br/>
                                <span class="logo_title mt-5"> Espace de connexion </span>

                </div>
                <div class="card-body">
                    <form action="{{ route('admin') }}" method="post">
                        @csrf
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="text" name="name" class="form-control" placeholder="Identifiant">
                        </div>

                        <div class="input-group form-group mt-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-key"></i></span>
                            </div>
                            <input type="password" name="password" class="form-control" placeholder="Mot de passe">
                        </div>

                        <div class="form-group mt-5">
                            <input type="submit" name="btn" value="Me connecter" class="btn btn-warning login_btn">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
