				<ul class="nav pcoded-inner-navbar ">

					<li class="nav-item">
					    <a href="index.html" class="nav-link ">
                            <span class="pcoded-micon">
                            <i class="feather icon-home"></i></span>
                            <span class="pcoded-mtext">Accueil</span></a>
					</li>

                    @if ($data['role'][0]== "SuperAdministrateur")
                        <li class="nav-item pcoded-menu-caption">
                            <label>Administrateurs</label>
                        </li>
                        <li class="nav-item pcoded-hasmenu">
                            <a href="#!" class="nav-link "><span class="pcoded-micon">
                                <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Administrateurs</span></a>
                            <ul class="pcoded-submenu">
                                <li><a href="{{ route('addadmin') }}" >Liste des administrateurs</a></li>
                                <li><a href="#" >Ajouter un administrateur Police</a></li>
                                <li><a href="#" >Ajouter un administrateur Gendarmerie</a></li>
                            </ul>
                        </li>
                    @endif

					@if ($data['role'][0]== "Administrateur")
                        @if ($data['information']["type"] == "Police")
                            <li class="nav-item pcoded-menu-caption">
                                <label>Sous Administrateurs</label>
                            </li>
                            <li class="nav-item pcoded-hasmenu">
                                <a href="#!" class="nav-link "><span class="pcoded-micon">
                                    <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Sous Administrateurs</span></a>
                                <ul class="pcoded-submenu">
                                    <li><a href="{{ route('addadmin') }}" >Liste des sous administrateurs de police</a></li>
                                    <li><a href="#" >Ajouter un sous administrateur Police</a></li>
                                </ul>
                            </li>
                            <li class="nav-item pcoded-menu-caption">
                                <label>Les postes</label>
                            </li>
                            <li class="nav-item pcoded-hasmenu">
                                <a href="#!" class="nav-link "><span class="pcoded-micon">
                                    <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Les postes</span></a>
                                <ul class="pcoded-submenu">
                                    <li><a href="{{ route('addadmin') }}" >Liste des postes de police</a></li>
                                    <li><a href="#" >Ajouter un poste de police</a></li>
                                </ul>
                            </li>

                        @endif

                        @if ($data['information']["type"] == "Gendarmerie")
                            <li class="nav-item pcoded-menu-caption">
                                <label>Sous Administrateurs</label>
                            </li>
                            <li class="nav-item pcoded-hasmenu">
                                <a href="#!" class="nav-link "><span class="pcoded-micon">
                                    <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Sous Administrateurs</span></a>
                                <ul class="pcoded-submenu">
                                    <li><a href="{{ route('addadmin') }}" >Liste des sous administrateurs de gendarmerie</a></li>
                                    <li><a href="#" >Ajouter un sous administrateur Gendarme</a></li>
                                </ul>
                            </li>
                            <li class="nav-item pcoded-menu-caption">
                                <label>Les postes</label>
                            </li>
                            <li class="nav-item pcoded-hasmenu">
                                <a href="#!" class="nav-link "><span class="pcoded-micon">
                                    <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Les postes</span></a>
                                <ul class="pcoded-submenu">
                                    <li><a href="{{ route('addadmin') }}" >Liste des postes de gendarmerie</a></li>
                                    <li><a href="#" >Ajouter un poste de gendarmerie</a></li>
                                </ul>
                            </li>

                        @endif

                    @endif

                    @if ($data['role'][0]== "SousAdministrateur")
                        @if ($data['information']["type"] == "Police")
                            <li class="nav-item pcoded-menu-caption">
                                <label>Controleurs</label>
                            </li>
                            <li class="nav-item pcoded-hasmenu">
                                <a href="#!" class="nav-link "><span class="pcoded-micon">
                                    <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Controleurs</span></a>
                                <ul class="pcoded-submenu">
                                    <li><a href="{{ route('addadmin') }}" >Liste des  Controleurs de police</a></li>
                                    <li><a href="#" >Ajouter un Controleur Police</a></li>
                                </ul>
                            </li>
                            <li class="nav-item pcoded-menu-caption">
                                <label>Localisation</label>
                            </li>
                            <li class="nav-item pcoded-hasmenu">
                                <a href="#!" class="nav-link "><span class="pcoded-micon">
                                    <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Localisation des controleurs</span></a>
                                <ul class="pcoded-submenu">
                                    <li><a href="{{ route('addadmin') }}" >Localisation</a></li>
                                </ul>
                            </li>

                            <li class="nav-item pcoded-menu-caption">
                                <label>Alertes</label>
                            </li>
                            <li class="nav-item pcoded-hasmenu">
                                <a href="#!" class="nav-link "><span class="pcoded-micon">
                                    <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Liste des alertes</span></a>
                                <ul class="pcoded-submenu">
                                    <li><a href="{{ route('addadmin') }}" >Alertes non resolus</a></li>
                                    <li><a href="{{ route('addadmin') }}" >Alertes resolus</a></li>
                                </ul>
                            </li>

                            <li class="nav-item pcoded-menu-caption">
                                <label>Observations</label>
                            </li>
                            <li class="nav-item pcoded-hasmenu">
                                <a href="#!" class="nav-link "><span class="pcoded-micon">
                                    <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Liste des observations</span></a>
                                <ul class="pcoded-submenu">
                                    <li><a href="{{ route('addadmin') }}" >Observations</a></li>
                                </ul>
                            </li>

                            <li class="nav-item pcoded-menu-caption">
                                <label>Messagerie</label>
                            </li>
                            <li class="nav-item pcoded-hasmenu">
                                <a href="#!" class="nav-link "><span class="pcoded-micon">
                                    <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Messagerie</span></a>
                                <ul class="pcoded-submenu">
                                    <li><a href="{{ route('addadmin') }}" >Messagerie</a></li>
                                </ul>
                            </li>

                        @endif

                        @if ($data['information']["type"] == "Gendarmerie")
                        <li class="nav-item pcoded-menu-caption">
                            <label>Controleurs</label>
                        </li>
                        <li class="nav-item pcoded-hasmenu">
                            <a href="#!" class="nav-link "><span class="pcoded-micon">
                                <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Controleurs</span></a>
                            <ul class="pcoded-submenu">
                                <li><a href="{{ route('addadmin') }}" >Liste des  Controleurs de gendarmerie</a></li>
                                <li><a href="#" >Ajouter un Controleur Gendarme</a></li>
                            </ul>
                        </li>
                        <li class="nav-item pcoded-menu-caption">
                            <label>Localisation</label>
                        </li>
                        <li class="nav-item pcoded-hasmenu">
                            <a href="#!" class="nav-link "><span class="pcoded-micon">
                                <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Localisation des controleurs</span></a>
                            <ul class="pcoded-submenu">
                                <li><a href="{{ route('addadmin') }}" >Localisation</a></li>
                            </ul>
                        </li>

                        <li class="nav-item pcoded-menu-caption">
                            <label>Alertes</label>
                        </li>
                        <li class="nav-item pcoded-hasmenu">
                            <a href="#!" class="nav-link "><span class="pcoded-micon">
                                <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Liste des alertes</span></a>
                            <ul class="pcoded-submenu">
                                <li><a href="{{ route('addadmin') }}" >Alertes non resolus</a></li>
                                <li><a href="{{ route('addadmin') }}" >Alertes resolus</a></li>
                            </ul>
                        </li>

                        <li class="nav-item pcoded-menu-caption">
                            <label>Observations</label>
                        </li>
                        <li class="nav-item pcoded-hasmenu">
                            <a href="#!" class="nav-link "><span class="pcoded-micon">
                                <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Liste des observations</span></a>
                            <ul class="pcoded-submenu">
                                <li><a href="{{ route('addadmin') }}" >Observations</a></li>
                            </ul>
                        </li>

                        <li class="nav-item pcoded-menu-caption">
                            <label>Messagerie</label>
                        </li>
                        <li class="nav-item pcoded-hasmenu">
                            <a href="#!" class="nav-link "><span class="pcoded-micon">
                                <i class="feather icon-layout"></i></span><span class="pcoded-mtext">Messagerie</span></a>
                            <ul class="pcoded-submenu">
                                <li><a href="{{ route('addadmin') }}" >Messagerie</a></li>
                            </ul>
                        </li>

                        @endif

                    @endif


				</ul>
